"use strict";

const healthEl = document.getElementById('health');
const dateEl = document.getElementById('date');

healthEl.innerHTML = 'Проверяем...';
dateEl.innerHTML = '';

chrome.runtime.sendMessage({'method':'reload'},function(response) 
{
  if(response.allRight) 
  {
    healthEl.innerHTML = 'p2p продажи работают!';
    healthEl.style.color = "green";
  }
  else 
  {
    let html = 'p2p продажи не работают!<br>Исправьте проблему:<br><br>';
    html += response.nextError;
    healthEl.innerHTML = html;
    healthEl.style.color = "red";
  }
  const date = new Date(parseInt(response.lastCheckTime));
  const formattedDate = date.toLocaleDateString('ru') + ' - ' + date.toLocaleTimeString('ru');
  dateEl.innerHTML = 'Последняя проверка: ' + formattedDate;
});